<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.regex.*" %>
<%@ page import="java.util.*" %>
<%!
public boolean pregMatch(String pattern, String content) {
	Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
	Matcher m = p.matcher(content);
	while (m.find()) {
		return true;
	}
    //return m.matches();
    return false;
}

public boolean mobile_check(String user_agent, String accept){
	//System.out.println("user_agent="+user_agent);
	//System.out.println("accept="+accept);
	boolean mobile_browser = false;

	if(pregMatch("(iPad|iPod|iPhone)", user_agent)){
		mobile_browser = true;
	}
	return mobile_browser;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<% 
	StringBuffer sb = new StringBuffer();
	sb.append("qihoo_id").append("%3D").append(request.getParameter("id"));
	for (Enumeration requestParameters = request.getParameterNames(); 
		 requestParameters.hasMoreElements(); ) {
	    String element = (String) requestParameters.nextElement();
	    String value = request.getParameter(element);
	    if (element.equals("id")) {
	        continue;
	    } else {
	    	sb.append("%26").append(element).append("%3D").append(value);
	    }
	} 
%>
<% if(mobile_check(request.getHeader("User-Agent"), request.getHeader("Accept"))) { %>	
	<meta http-equiv="refresh" content="0;url=http://m.onelink.me/467da8c/" />
<% } else { %>	
	<meta http-equiv="refresh" content="0;url=http://m.onelink.me/638dd144" />
<% } %>
	<title>360 store</title>
</head>

</html>
