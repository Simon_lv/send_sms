<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width"/>
<link href="${ctx}/css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/js/jquery.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery-ui.js"></script>

</head>

<body>
<div class="top">檢測表
</div>
<div class="main">
<div class="box">
        <div class="ct r_20">
   <c:forEach var="chk" items="${data}">
	   <h1>
	   <c:choose>
			<c:when test="${chk.pid == '1'}">
				15點  剩餘 : ${chk.cnt }
			</c:when>
			<c:when test="${chk.pid == '2'}">
				50點  剩餘 : ${chk.cnt }
			</c:when>
			<c:when test="${chk.pid == '3'}">
				40點  剩餘 : ${chk.cnt }
			</c:when>
			<c:when test="${chk.pid == '4'}">
				140點  剩餘 : ${chk.cnt }
			</c:when>
	   </c:choose>
	   </h1>
	   <br/>         
    </c:forEach>
	</div>
</div>
</div>
</body>
</html>
