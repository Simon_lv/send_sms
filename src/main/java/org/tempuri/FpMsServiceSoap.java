/**
 * FpMsServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface FpMsServiceSoap extends java.rmi.Remote {
    public java.lang.String sendSMSwithUserData(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, boolean isLong, java.lang.String data1, java.lang.String data2, java.lang.String data3, java.lang.String data4) throws java.rmi.RemoteException;
    public java.lang.String sendSMS(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, boolean isLong) throws java.rmi.RemoteException;
    public java.lang.String sendDyncSMSwithUserData(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong, java.lang.String data1, java.lang.String data2, java.lang.String data3, java.lang.String data4) throws java.rmi.RemoteException;
    public java.lang.String sendDyncSMS(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong) throws java.rmi.RemoteException;
    public java.lang.String sendDyncSMSwithUserDataAndPriority(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong, java.lang.String data1, java.lang.String data2, java.lang.String data3, java.lang.String data4, boolean hiPriority) throws java.rmi.RemoteException;
    public java.lang.String sendDyncSMSWithPriority(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong, boolean hiPriority) throws java.rmi.RemoteException;
    public java.lang.String querySMS(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException;
    public java.lang.String SMSQueryDr(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException;
    public java.lang.String querySMSWithSendTime(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException;
    public java.lang.String querySMSStatusByTimeRange(java.lang.String user, java.lang.String pass, java.lang.String startTime, java.lang.String endTime) throws java.rmi.RemoteException;
    public java.lang.String queryReceivedSMS(java.lang.String user, java.lang.String pass, java.lang.String cellId) throws java.rmi.RemoteException;
    public java.lang.String queryReceivedSMSByTimeRange(java.lang.String user, java.lang.String pass, java.lang.String cellId, java.lang.String startTime, java.lang.String endTime) throws java.rmi.RemoteException;
    public java.lang.String queryUserPoint(java.lang.String user, java.lang.String pass) throws java.rmi.RemoteException;
    public java.lang.String scheduleSMSCancel(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException;
}
