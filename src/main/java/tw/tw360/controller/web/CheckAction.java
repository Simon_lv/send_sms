package tw.tw360.controller.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import tw.tw360.controller.BaseController;
import tw.tw360.service.PrizeSnManager;

/**
 * 獎品
 *
 */

@Controller
@Scope("session")
@RequestMapping("/check")
public class CheckAction extends BaseController {
	
	private static final String PATH = "check/";

	@Autowired
	private PrizeSnManager prizeSnManager;
	
	@RequestMapping(value = "/list")
    public String list(Model m, HttpSession session) 
	{
		List<Map<String, Object>> list = prizeSnManager.countByPrize();
		
		m.addAttribute("data", list);
        return PATH + "list";
    }
	
}
