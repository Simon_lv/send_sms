package tw.tw360.controller.api;

import java.io.StringReader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tempuri.FpMsServiceLocator;
import org.tempuri.FpMsServiceSoap;






import com.sun.org.apache.xml.internal.security.utils.Base64;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.SMSRecord;
import tw.tw360.dto.ZgActivity;
import tw.tw360.service.SMSRecordManager;
import tw.tw360.service.SendSMSManager;
import tw.tw360.utils.SMSUtils;






@Controller
public class SendSMSApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(SendSMSApi.class);
	
	@Autowired
	private SMSRecordManager smsRecordManager;
	
	@Autowired
	private SendSMSManager sendSmsManager;
	
	private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	 
//	private final String content1 = "感謝您預約「幻想編年史」,憑序號%s即可獲得萌寵「粉紅草尼馬」一隻~請點擊網址兌換http://goo.gl/XzKg5G";
//	private final String content1 = "手機遊戲《決戰天龍》多謝您完成事前登錄，禮包序號:%s。快D下載http://goo.gl/wlbjnE";
//	private final String content1 = "《決戰天龍》正式上架囉!感謝預約，禮包序號:%s。請點擊下載喔http://goo.gl/9jT3L4";
//	private final String content1 = "《決戰天龍》全民激戰傳說武將免費送!序號:%s，點擊下載兌換http://goo.gl/9jT3L4";
//	private final String content1 = "勇者們,《幻想編年史》緋紅之歌,冒險再開.全新職業\"槍械師\"等你冒險!和平消逝,新的危機降臨!詳情:http://goo.gl/0OxKxN";
//	private final String content1 = "感謝預約SS級彈射手遊【聖靈衝擊】輸入%s領虛寶！立刻下載：http://goo.gl/VlG3YW"; //TW
//	private final String content1 = "感謝預約SS級彈射手遊【聖靈衝擊】輸入%s領虛寶！立刻下載：http://goo.gl/o3B3CL";
//	private final String content1 = "感謝預約【亂鬥堂2：最強英雄】快下載並創角~您的預約獎勵序號為：%s 遊戲下載 http://goo.gl/BZwKcF";
//	private final String content1 = "感謝主人預約「崩壞學園2」手遊登錄活動(^_^~)獎勵虛寶：5G5TK8 遊戲下載http://goo.gl/0RxPxv 一起來玩吧！"; //新 
//	private final String content1 = "《暗黑黎明2》9/28 零點十分開放！輸入%s拿預約好禮，下載http://goo.gl/ixdUr4"; //TW
//	private final String content2 = "倩女預約達標獎勵!髮飾 %s,時裝 %s,咩咩坐騎 %s"; //TW時裝
	private final String content2 = "記得拎埋倩女預約獎勵！髮飾%s，時裝%s，咩咩坐騎%s"; //HK時裝
//	private final String content1 = "《暗黑黎明2》今日開放！輸入%s拿獨家虛寶，立即下載 http://goo.gl/ixdUr4"; //TW VIP
//	private final String content1 = "《暗黑黎明2》今日開放！輸入%s拿獨家虛寶，立即下載 http://goo.gl/dmllSn"; //HK VIP
//	private final String content1 = "感謝您的預約!《倩女幽魂》今日中午開放,憑序號%s拿好禮,立即下載 https://goo.gl/aYqpfy"; //TW

//	private final String content1 = "恭喜您在倩女活動抽中GASH150點！序號%s，下載玩倩女https://goo.gl/aYqpfy"; //150 TW
//	private final String content1 = "恭喜您在倩女活動抽中GASH150點！序號%s，下載玩倩女https://goo.gl/GvfE1a"; //150 HK
	private final String content1 = "恭喜您在倩女活動抽中GASH1000點！序號%s，下載玩倩女https://goo.gl/aYqpfy"; //1000
//	private final String content1 = "香港經典《倩女幽魂》嚟喇！快啲同小倩一齊玩～拎埋虛寶%s，即刻下載 https://goo.gl/GvfE1a 獨家著數送俾你！髮飾 %s、時裝 %s、咩咩坐騎 %s"; //HK VIP
//	private final String androidUrl = "http://m.onelink.me/619546c0";
//	private final String iosUrl = "http://m.onelink.me/38575821";
//	private final String retUrl = "http://www.8888play.com/cbhk/ms.jsp";
	private final String twUrl = "http://goo.gl/0OxKxN";
	private final String hkUrl = "http://goo.gl/aYuXC7";
//	private final String content2 = "恭喜獲得決戰天龍預約GASH獎項!序號%s 遊戲下載連結 http://goo.gl/0P4P8F";
//	private final String content2 = "恭喜獲得決戰天龍預約GASH獎項!序號%s 遊戲下載連結 http://goo.gl/fJVdms";
	@RequestMapping(value = "send", produces = "application/json")
	public @ResponseBody Map send(
			@RequestParam(value = "device", required = false, defaultValue = "") String device)
//			@RequestParam("device") String device)
	{	
		LOG.info("============== SendSMSApi send ====================");
		LOG.info("device="+device);
		String code = Config.RETURN_SUCCESS;
		String platform = "8888play";
		Map result = new HashMap();
		String vcode = "";
		String sno = "dx";
		int begin = 1;
		String url = "";
		try {
//			Map<String, String> textMap = new HashMap<String, String>();
//			textMap.put("VIP", "VIP享免費1人入場(市值500)");
//			textMap.put("黃金VIP", "黃金VIP享免費2人入場(市值1,000)");
//			textMap.put("白金VIP", "白金VIP享免費4人入場(市值2,000)");
//			textMap.put("鑽石VIP", "鑽石VIP享免費4人入場(市值2,000)");
//			textMap.put("至尊VIP", "至尊VIP享免費10人入場(市值5,000)");
//			if ("android".equals(device)) {
//				url = androidUrl;
//			} else if ("ios".equals(device)) {
//				url = iosUrl;
//			}
			url = twUrl;
			List<ZgActivity> mobileList = sendSmsManager.queryAll(device);
			for (ZgActivity zga : mobileList) {
				begin++;
				if ("hk".equals(device)) {
					url = hkUrl;
				}
				if (StringUtils.isNotBlank(zga.getGameSn())) {
//					String text = textMap.get(zga.getGameSn()); 
//					String content3 = String.format(content1, text, url);
					String content3 = String.format(content1, zga.getGameSn(), zga.getGashSn(), zga.getGashSn1(), zga.getGashSn2(), url);
					System.out.println("content3="+content3);
				
					Map<String, String> retHM = this.sendSMS(content3,
							zga.getMobile(), "7200");
				
					String theSmsRetStatus = retHM.get("status");
					
					if (theSmsRetStatus != null) {
						if (theSmsRetStatus.equals("0")) {
							System.out.println("retSMSSuccess");
							sendSmsManager.updateStatus(zga.getId().intValue(), 2);
						}
					}
					Thread.sleep(500);
				}	
				if (false && StringUtils.isNotBlank(zga.getGashSn())) {
					begin++;
//					String content4 = String.format(content2, zga.getGashSn(), zga.getPassword());
					String content4 = String.format(content2, zga.getGashSn(), zga.getGashSn1(), zga.getGashSn2(), url);
					System.out.println("content4="+content4);
					Map<String, String> retHM1 = this.sendSMS(content4,
							zga.getMobile(), "7200");
					String theSmsRetStatus1 = retHM1.get("status");
					
					if (theSmsRetStatus1 != null) {
						if (theSmsRetStatus1.equals("0")) {
							System.out.println("retSMSSuccess");
							sendSmsManager.updateStatus(zga.getId().intValue(), 2);
						}
					}
					Thread.sleep(500);
				}
				Thread.sleep(100);
			}
//			String content = "感謝預約仙境之戀2,獎勵序號:1jltlo6quc404。請點擊網址兌換http://118.163.80.221:8010/ms.jsp";
//			ZgActivity zga = new ZgActivity();
//			zga.setId(0L);
//			zga.setMobile("0988013239");
//			Thread t = new Thread(new SmsThread("360", content, zga, 15, sendSmsManager, smsRecordManager));
//			t.start();
//			
//			ZgActivity zga1 = new ZgActivity();
//			zga1.setId(0L);
//			zga1.setMobile("0988013239");
//			content = "感謝預約仙境之戀2，恭喜抽得GASH 50點卡號:1051503798,密碼:57640P6CW8GFNR4CXCE8E,登入再抽100點!";
//			Thread t1 = new Thread(new SmsThread("360", content, zga1, 16, sendSmsManager, smsRecordManager));
//			t1.start();
			
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		} catch (Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		LOG.info("result="+result);
		return result;
	}
	
	@RequestMapping(value = "sendYo", produces = "application/json")
	public @ResponseBody Map sendYo(
			@RequestParam(value = "device", required = false, defaultValue = "") String device)
//			@RequestParam("device") String device)
	{	
		LOG.info("============== SendSMSApi sendYo ====================");
		LOG.info("device="+device);
		String code = Config.RETURN_SUCCESS;
		String platform = "8888play";
		Map result = new HashMap();
		String vcode = "";
		String sno = "FT";
		int begin = 1;
		String url = "";
		try {
//			if ("android".equals(device)) {
//				url = androidUrl;
//			} else if ("ios".equals(device)) {
//				url = iosUrl;
//			}
			url = twUrl;
			List<ZgActivity> mobileList = sendSmsManager.queryAll(device);
			for (ZgActivity zga : mobileList) {
				begin++;
				if ("hk".equals(device)) {
					url = hkUrl;
				}
				if (StringUtils.isNotBlank(zga.getGameSn())) {
					String content3 = String.format(content1, zga.getGameSn(), url);
//					String content3 = String.format(content1, url);
					System.out.println("content3="+content3);
					Thread t = new Thread(new SmsThread(platform, content3, zga, sno+begin, sendSmsManager, smsRecordManager));
					t.start();
					Thread.sleep(500);
				}	
				if (StringUtils.isNotBlank(zga.getGashSn())) {
					begin++;
//					String content4 = String.format(content2, zga.getGashSn(), zga.getPassword());
					String content4 = String.format(content2, zga.getGashSn(), zga.getGashSn1(), zga.getGashSn2(), url);
					System.out.println("content4="+content4);
					Thread t1 = new Thread(new SmsThread(platform, content4, zga, sno+begin, sendSmsManager, smsRecordManager));
					t1.start();
					Thread.sleep(500);
				}
				Thread.sleep(800);
			}
//			String content = "感謝預約仙境之戀2,獎勵序號:1jltlo6quc404。請點擊網址兌換http://118.163.80.221:8010/ms.jsp";
//			ZgActivity zga = new ZgActivity();
//			zga.setId(0L);
//			zga.setMobile("0988013239");
//			Thread t = new Thread(new SmsThread("360", content, zga, 15, sendSmsManager, smsRecordManager));
//			t.start();
//			
//			ZgActivity zga1 = new ZgActivity();
//			zga1.setId(0L);
//			zga1.setMobile("0988013239");
//			content = "感謝預約仙境之戀2，恭喜抽得GASH 50點卡號:1051503798,密碼:57640P6CW8GFNR4CXCE8E,登入再抽100點!";
//			Thread t1 = new Thread(new SmsThread("360", content, zga1, 16, sendSmsManager, smsRecordManager));
//			t1.start();
			
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		} catch (Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		LOG.info("result="+result);
		return result;
	}
	
	 private static class SmsThread implements Runnable {
		 private String platform;
		 private String content;
		 private ZgActivity zga;
		 private String sno;
		 private SendSMSManager sendSmsManager;
		 private SMSRecordManager smsRecordManager;
		 
		 public SmsThread(String platform, String content, ZgActivity zga, String sno, SendSMSManager sendSmsManager, SMSRecordManager smsRecordManager) {
			 this.platform = platform;
			 this.content = content;
			 this.zga = zga;
			 this.sendSmsManager = sendSmsManager;
			 this.smsRecordManager = smsRecordManager;
			 this.sno = sno;
		 }
		 
		 public void run() {
			 HashMap<String, String> retHM = this.sendVcodeToMobile(platform, content,
						zga.getMobile(), sno);
				String theSmsRetStatus = retHM.get("status");
	
				if (theSmsRetStatus != null) {
					if (theSmsRetStatus.equals("0")) {
						System.out.println("retSMSSuccess");
						sendSmsManager.updateStatus(zga.getId().intValue(), 2);
					}
				}
		 }
		 
		 private HashMap<String, String> sendVcodeToMobile(String platform,
					String msg, String mobile, String sno) {
			    if (mobile.startsWith("+88609")) {
			    	mobile = mobile.substring(4);
			    } else if (mobile.startsWith("+8869")) {
				    mobile = "0"+mobile.substring(4);	
			    } else if (mobile.startsWith("+852") || mobile.startsWith("+853")) {
			    	mobile = mobile.substring(1);
			    }
			    System.out.println("SendSMSApi sendVcodeToMobile mobile===="+mobile);
				SMSUtils sms = new SMSUtils();
				String smsRetStr = sms.sendSMS(
						false,  //測試環境請設 true
						mobile,
						msg,
						platform, 
						sno + sdf.format(new Date())
						);
				//System.out.println("認證碼:" + vcode);
				System.out.println("smsTestRet:" + smsRetStr);
				HashMap<String, String> retHM = SMSUtils.getHashMapFromStr(smsRetStr);

				String retStatus = retHM.get("status");
//				if (retStatus != null) {
					System.out.println("sendVcodeToMobile retStatus="+retStatus);
//					if (retStatus.equals("0")) {
						SMSRecord smsRecord = new SMSRecord();
						smsRecord.setMessageId(retHM.get("MessageID"));
						smsRecord.setUsedCredit(retHM.get("UsedCredit"));
						smsRecord.setMemberId(retHM.get("MemberID"));
						smsRecord.setCredit(retHM.get("Credit"));
						smsRecord.setMobileNo(mobile);
						smsRecord.setStatus(retHM.get("status"));
						smsRecord.setCreateTime(new Timestamp(System
								.currentTimeMillis()));

						smsRecordManager.add(smsRecord);
//					}
//				}

				return retHM;
			}
	 }
	 
	// 接收 yoyo8 簡訊公司的回call
		@RequestMapping(value = "/reportSMS")
		public String reportSMS(
				@RequestParam(value = "status") String status,
				@RequestParam(value = "MemberID", required = false) String memberID,
				@RequestParam(value = "MessageID", required = false) String messageID,
				@RequestParam(value = "UsedCredit", required = false) String usedCredit,
				@RequestParam(value = "Credit", required = false) String credit,
				@RequestParam(value = "MobileNo", required = false) String mobileNo,
				@RequestParam(value = "SourceProdID", required = false) String sourceProdID,
				@RequestParam(value = "SourceMsgID", required = false) String sourceMsgID) {
			System.out.println("getReport status=" + status);

			if (status.equals("0")) {
				if (messageID != null) {
					SMSRecord retRecord = smsRecordManager
							.findByMessageId(messageID);
					if (sourceMsgID == null) {
						sourceMsgID = "";
					}
					if (retRecord != null) {
						Timestamp nowTime = new Timestamp(
								System.currentTimeMillis());
						System.out.println("getReport " + messageID + " " + status
								+ " " + sourceMsgID + " " + nowTime);
						smsRecordManager.updateReport(messageID, status,
								sourceProdID, sourceMsgID, nowTime);
					}
				}

			}

			String nextPage = "/reportSMS";
			return nextPage;
		}
		
		private final String smsUser = "8888ads";
		private final String smsPass = "8888ads";
		private final SimpleDateFormat msdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		private final String reportUrl = "http://www.raidcall.com.tw/api/sms/result.php";
		/*
		 * 銓力回覆
		 */
		@RequestMapping(value = "/smsResult", method = RequestMethod.POST)
		public String smsResult(@RequestBody String requestBody) throws Exception {
			 LOG.info("============== SendSMSApi smsResult ====================");
			 LOG.info("requestBody="+requestBody);
			 SAXBuilder bSAX = new SAXBuilder(false);
			 Document docJDOM = bSAX.build(new StringReader(requestBody));
			 Element elmtRoot = docJDOM.getRootElement();  
			 List lstSmsStatus = elmtRoot.getChildren("SmsStatus");
			 for (int ii = 0; ii<lstSmsStatus.size(); ii++) {
				 Element elmtChild = (Element) lstSmsStatus.get(ii);
				 String seq = elmtChild.getChildText("SEQ");
				 String mobile = elmtChild.getChildText("DestAddress");
				 String status = elmtChild.getChildText("RES");
				 String checkDate = elmtChild.getChildText("CheckDate");
				 if (StringUtils.isNotBlank(seq)) {
					 SMSRecord retRecord = smsRecordManager
								.findByMessageId(seq);
						
					if (retRecord != null) {
							Timestamp nowTime = new Timestamp(
									System.currentTimeMillis());
							System.out.println("getReport " + seq + " " + status
									 + " " + nowTime);
							smsRecordManager.updateReport(seq, status,
									smsUser, checkDate, nowTime);
					}
				}
				 
			}
			 
		 	String nextPage = "/smsResult";
			return nextPage;
		 }
		
		/*
		 * 銓力簡訊
		 */
		private Map<String, String> sendSMS(
				String msg, String mobile, String validSec) throws Exception {
		    if (mobile.startsWith("+88609")) {
		    	mobile = mobile.substring(4);
		    } else if (mobile.startsWith("8869")) {	
		    	mobile = "0"+mobile.substring(3);
		    } else if (mobile.startsWith("88609")) {	
		    	mobile = mobile.substring(3);	
		    } else if (mobile.startsWith("+852") || mobile.startsWith("+853")) {
		    	//mobile = mobile.substring(1);
		    } else if (mobile.startsWith("852") || mobile.startsWith("853")) {
		    	mobile = "+"+mobile;
		    }
		    FpMsServiceLocator fpMsServiceLocator = new FpMsServiceLocator();

			FpMsServiceSoap stub = fpMsServiceLocator.getfpMsServiceSoap();
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.SECOND, Integer.parseInt(validSec));
			String response = stub.sendSMS(mobile, msg, smsUser, Base64.encode(smsPass.getBytes()), "", msdf.format(cal.getTime()), false);
			System.out.println("sendSms response: " + response);
			SAXBuilder bSAX = new SAXBuilder(false);
			Document docJDOM = bSAX.build(new StringReader(response));
			Element elmtRoot = docJDOM.getRootElement();  
			String seqNo = elmtRoot.getChildText("SEQ");
			String errNo = elmtRoot.getChildText("ERR");
			Map<String, String> retHM = new HashMap<String, String>();
			retHM.put("MessageID",seqNo);
			if ("0".equals(errNo)) {
				retHM.put("UsedCredit","1");
			}
			retHM.put("status", errNo);
			SMSRecord smsRecord = new SMSRecord();
			smsRecord.setMessageId(retHM.get("MessageID"));
			smsRecord.setUsedCredit(retHM.get("UsedCredit"));
			smsRecord.setMemberId(smsUser);
			smsRecord.setCredit("");
			smsRecord.setMobileNo(mobile);
			smsRecord.setStatus(retHM.get("status"));
			smsRecord.setCreateTime(new Timestamp(System
					.currentTimeMillis()));

			smsRecordManager.add(smsRecord);

			return retHM;
		}	
	
	public static void main(String[] arg) {
		final String content1 = "感謝預約仙境之戀2,獎勵序號:%s。請點擊網址兌換http://118.163.80.221:8010/ms.jsp";
		
		final String content2 = "感謝預約仙境之戀2，恭喜抽得GASH 50點卡號:%s,密碼:%s,登入再抽100點!";
		System.out.println(String.format(content1, "1234"));
		System.out.println(String.format(content2, "45678","fdsdsgsdfcds"));
	}
		
	
}
