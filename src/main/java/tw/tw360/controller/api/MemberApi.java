package tw.tw360.controller.api;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.tw360.constants.Config;
import tw.tw360.controller.BaseController;
import tw.tw360.dto.ViewCount;
import tw.tw360.service.ViewCountManager;






@Controller
@Scope("session")
public class MemberApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(MemberApi.class);
	@Autowired
	private ViewCountManager ccManager;
	
	@RequestMapping(value = "click", produces = "application/json")
	public @ResponseBody Map click(@RequestParam("platform") String platform)
	{	
		LOG.info("============== MemberApi click ====================");
		LOG.info("platform="+platform);
		
		String code = Config.RETURN_SUCCESS;
		Map result = new HashMap();
		try {
			ViewCount cc = ccManager.findByPlatform(platform);
			if (cc == null) {
				cc = new ViewCount();
				cc.setPlatform(platform);
				cc.setCount(1);
				cc.setCreateTime(new Timestamp(System.currentTimeMillis()));
				ccManager.add(cc);
			} else {
				cc.setCount(cc.getCount()+1);
				ccManager.updateCount(platform, cc.getCount());
			}

			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		} catch (Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		LOG.info("result="+result);
		return result;
	}
	
	
}
