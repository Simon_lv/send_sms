package tw.tw360.controller;


import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import tw.tw360.common.encrypt.MD5Util;
import tw.tw360.dto.Member;
import tw.tw360.service.MemberManager;


public class BaseController {

	@Autowired
	protected MemberManager memberManager;
	
	protected String OFFERWALL_LOGIN_MEMBER = "offerwall_login_member";
	protected String OFFERWALL_PLATFORM = "offerwall_platform";
	
	
	protected boolean checkLogin(HttpSession session) {
		if (session.getAttribute(OFFERWALL_LOGIN_MEMBER) == null) {
			return false;
		}
		return true;
	}
	
	protected boolean checkWebViewToken(String id, String timestamp, 
			String pgName, String imei,int result, String token) {
		
		if (StringUtils.isBlank(token)) {
			return false;
		}
		String param = id + timestamp + pgName+imei+","+result;
		System.out.println("checkWebViewToken param="+param);
		System.out.println("checkWebViewToken token="+token);
		System.out.println("checkWebViewToken new token="+MD5Util.crypt(param));
		return (token.equalsIgnoreCase(MD5Util.crypt(param)));
		/*
		String param = id + timestamp + pgName+imei+","+result;
		System.out.println("checkWebViewToken id="+id);
		System.out.println("checkWebViewToken timestamp="+timestamp);
		System.out.println("checkWebViewToken pgName="+pgName);
		System.out.println("checkWebViewToken imei="+imei);
		System.out.println("checkWebViewToken result="+result);
		System.out.println("checkWebViewToken token="+token);
		System.out.println("checkWebViewToken new token="+MD5Util.crypt(param));
		return true;
		*/
	}
	
	protected boolean checkWebViewToken(String id, String timestamp, 
			String imei,String uid, String token) {
		
		if (StringUtils.isBlank(token)) {
			return false;
		}
		String param = id + timestamp + imei + uid;
		System.out.println("checkWebViewToken param="+param);
		System.out.println("checkWebViewToken token="+token);
		System.out.println("checkWebViewToken new token="+MD5Util.crypt(param));
		return (token.equalsIgnoreCase(MD5Util.crypt(param)));
		/*
		String param = id + timestamp + pgName+imei+","+result;
		System.out.println("checkWebViewToken id="+id);
		System.out.println("checkWebViewToken timestamp="+timestamp);
		System.out.println("checkWebViewToken pgName="+pgName);
		System.out.println("checkWebViewToken imei="+imei);
		System.out.println("checkWebViewToken result="+result);
		System.out.println("checkWebViewToken token="+token);
		System.out.println("checkWebViewToken new token="+MD5Util.crypt(param));
		return true;
		*/
	}
	
	protected void saveLoginSession(HttpSession session, Member member) 
	{
		session.setAttribute(OFFERWALL_LOGIN_MEMBER, member);
	}	
	
	protected void savePlatformSession(HttpSession session, String platform) 
	{
		session.setAttribute(OFFERWALL_PLATFORM, platform);
	}
	
	protected Member getLoginSession(HttpSession session) 
	{
		return (Member)session.getAttribute(OFFERWALL_LOGIN_MEMBER);
	}
	
	protected String getPlatformSession(HttpSession session) 
	{
		return (String)session.getAttribute(OFFERWALL_PLATFORM);
	}
	
	protected void invaldateMemberCache(Member member) {
//		memberManager.invaldateCacheById(member.getId());
//		memberManager.invaldateCacheByUsername(member.getUsername());
	}
	
	protected String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			if(ip.equals("127.0.0.1")){
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				ip = inet.getHostAddress();
			}
		}
		return ip.indexOf(",") > -1 ? ip.substring(0, ip.indexOf(",")) : ip;
	}
	
	public static void main(String[] arg) {
		System.out.println("checkWebViewToken new token="+MD5Util.crypt("exchange.do"));
	}
}
