package tw.tw360.dto;

import java.sql.Timestamp;

public class ViewCount extends BaseDto {
	
	private String platform;
	private int count;
	private Timestamp createTime;
	
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	

}
