package tw.tw360.dto;

import java.sql.Timestamp;

/**
 * 會員資料
 * */
public class Member extends BaseDto{
	 
	public static final int STATUS_VALID = 1;
	public static final int STATUS_WAITING_FOR_VERIFICATION = 2;
	public static final int STATUS_SUSPENDED = 3;
	public static final int STATUS_INVALID = 4;
	
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String account;
	private String platform;
	private String password;
	private String name;
	private String email;
	private String address;
	private String mobile;
	private String vcode;
	private Timestamp createTime;
	private Timestamp lastLoginTime;
	private int loginCount;
	private int status;
	private String androidId;
	private String deviceImei;
	private String deviceMac;
	private int bonus;
	private int twdMoney;
	private float usdMoney;
	private float cnyMoney;
	private Timestamp vcodeTime;
	private int loginErrCount;
	private Timestamp loginErrTime;
	private Timestamp registerTime;
	private String recommCode;
	private String friendCode;
	
	public Timestamp getVcodeTime() {
		return vcodeTime;
	}
	public void setVcodeTime(Timestamp vcodeTime) {
		this.vcodeTime = vcodeTime;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Timestamp getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Timestamp lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public int getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getAndroidId() {
		return androidId;
	}
	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}
	public String getDeviceImei() {
		return deviceImei;
	}
	public void setDeviceImei(String deviceIMEI) {
		this.deviceImei = deviceIMEI;
	}
	public String getDeviceMac() {
		return deviceMac;
	}
	public void setDeviceMac(String deviceMac) {
		this.deviceMac = deviceMac;
	}
	public int getBonus() {
		return bonus;
	}
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}
	public int getTwdMoney() {
		return twdMoney;
	}
	public void setTwdMoney(int twdMoney) {
		this.twdMoney = twdMoney;
	}
	public float getUsdMoney() {
		return usdMoney;
	}
	public void setUsdMoney(float usdMoney) {
		this.usdMoney = usdMoney;
	}
	public float getCnyMoney() {
		return cnyMoney;
	}
	public void setCnyMoney(float cnyMoney) {
		this.cnyMoney = cnyMoney;
	}
	public int getLoginErrCount() {
		return loginErrCount;
	}
	public void setLoginErrCount(int loginErrCount) {
		this.loginErrCount = loginErrCount;
	}
	public Timestamp getLoginErrTime() {
		return loginErrTime;
	}
	public void setLoginErrTime(Timestamp loginErrTime) {
		this.loginErrTime = loginErrTime;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public Timestamp getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Timestamp registerTime) {
		this.registerTime = registerTime;
	}
	public String getRecommCode() {
		return recommCode;
	}
	public void setRecommCode(String recommCode) {
		this.recommCode = recommCode;
	}
	public String getFriendCode() {
		return friendCode;
	}
	public void setFriendCode(String friendCode) {
		this.friendCode = friendCode;
	}
}
