package tw.tw360.dto;

public class DailyLoginRecord extends BaseDto {

	private static final long serialVersionUID = 1L;


	private String loginCurrentDate;
	private Long memberId;
	private int loginCount;
	private String platform;

	public String getLoginCurrentDate() {
		return loginCurrentDate;
	}

	public void setLoginCurrentDate(String loginCurrentDate) {
		this.loginCurrentDate = loginCurrentDate;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}


}
