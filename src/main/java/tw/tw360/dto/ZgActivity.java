package tw.tw360.dto;

import java.sql.Timestamp;
/**
 * 簡訊紀錄
 * */
public class ZgActivity extends BaseDto{
	
	private String 		mobile;
	private String 		email;
	private String 		gameSn;
	private String 		gashSn;
	private String 		gashSn1;
	private String 		gashSn2;
	private String 		password;
	private String 		device;
	private String 		status;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGameSn() {
		return gameSn;
	}
	public void setGameSn(String gameSn) {
		this.gameSn = gameSn;
	}
	public String getGashSn() {
		return gashSn;
	}
	public void setGashSn(String gashSn) {
		this.gashSn = gashSn;
	}
	public String getGashSn1() {
		return gashSn1;
	}
	public void setGashSn1(String gashSn1) {
		this.gashSn1 = gashSn1;
	}
	public String getGashSn2() {
		return gashSn2;
	}
	public void setGashSn2(String gashSn2) {
		this.gashSn2 = gashSn2;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
