package tw.tw360.dto;

import java.sql.Timestamp;

public class PrizeSn extends BaseDto{
	public static final int STATUS_VALID = 1;
	public static final int STATUS_INVALID = 2; 

	private static final long serialVersionUID = 1L;
	
	private int PrizeId;
	private String sn;
	private String password;
	private Timestamp startTime;
	private Timestamp endTime;
	private Timestamp createTime;
	private int status;
	public int getPrizeId() {
		return PrizeId;
	}
	public void setPrizeId(int prizeId) {
		PrizeId = prizeId;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
