package tw.tw360.constants;

import java.util.HashMap;

public class Config {
	
	public final static int PAGE_SIZE = 20;
	
	public final static String PLATFORM_FACEBOOK = "facebook";
	public final static String PLATFORM_GOOGLE = "google";
	
	public final static int SIGNIN_BEFORE_DOWNLOAD = 1; //1.按下下載
	public final static int SIGNIN_BEGIN_DOWNLOAD = 2; //2.開始下載
	public final static int SIGNIN_FINISH_DOWNLOAD = 3; //3.下載完成
	public final static int SIGNIN_BEGIN_INSTALL = 4; //4.開始安裝
	public final static int SIGNIN_FINISH_INSTALL = 5; //5.安裝完成
	public final static int SIGNIN_ADD_ICON = 6; //6.放置icon
	public final static int SIGNIN_APP_ACTIVE = 7; //7.激活
	public final static int SIGNIN_APP_LOGIN = 8; //8.登入
	public final static int SIGNIN_APP_CLOSE = 9; //9.關閉
	
	public static final String RETURN_SUCCESS = "0000";
	public static final String RETURN_NOT_FOUND = "0001";
	public static final String RETURN_INPUT_ERROR = "0002";
	public static final String RETURN_LOGIN_FAIL = "0003";
	public static final String RETURN_DATA_EXIST = "0004";
	public static final String RETURN_TOKEN_FAIL = "0005";
	public static final String RETURN_MOBILE_FORMATE_ERROR = "0006";
	public static final String RETURN_EMAIL_FORMATE_ERROR = "0007";
	public static final String RETURN_SYSTEM_ERROR = "9999";
	public static final HashMap RETURN_RESULT=new HashMap();
	static{
		RETURN_RESULT.put(RETURN_SUCCESS, "success");
		RETURN_RESULT.put(RETURN_NOT_FOUND, "data not found");
		RETURN_RESULT.put(RETURN_INPUT_ERROR, "data input error");
		RETURN_RESULT.put(RETURN_LOGIN_FAIL, "login fail");
		RETURN_RESULT.put(RETURN_DATA_EXIST, "data exist");
		RETURN_RESULT.put(RETURN_TOKEN_FAIL, "check token fail");
		RETURN_RESULT.put(RETURN_MOBILE_FORMATE_ERROR, "mobile format error");
		RETURN_RESULT.put(RETURN_EMAIL_FORMATE_ERROR, "email format error");
		RETURN_RESULT.put(RETURN_SYSTEM_ERROR, "system error");
	}
	
	public static final String MAIL_HOST="積分牆網站";
	public static final String FORGET_PASSWORD_SUBJECT="【重要】360積分牆帳號密碼修改";
	public static final String FORGET_PASSWORD_CONTENT="<br>&nbsp;您好，這是一封重要郵件，您的新密碼為  %s ，請您妥善保管好新密碼！<br><br>如您有其他問題，請聯繫我們：cs@360tw.tw";
}
