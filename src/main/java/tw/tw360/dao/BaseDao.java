package tw.tw360.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springside.modules.orm.Page;

import tw.tw360.dto.BaseDto;



public class BaseDao<T> extends JdbcDaoSupport {

	private static final Logger LOG = LoggerFactory.getLogger(BaseDao.class);
	
	private String fetchCombineSqlString(final String sql, final Object[] args){
		if(args==null || args.length==0)
			return sql;
		
		String result = null;
		Object[] params = new Object[args.length];		
		for(int i=0; i<args.length; i++){
			Object obj = args[i];
			if(obj instanceof Integer || obj instanceof Long 
					|| obj instanceof Float || obj instanceof Double){
				params[i] = obj;
			}else{
				params[i] = "'" + obj + "'";
			}			
		}
		
		try{
			result = String.format(sql.replace("?", "%s"), params);
		}catch(Exception e){
			result = sql;
		}
		
		return result;
	}
	
	protected Object queryForObject(final DataSource dataSource, final String sql, final Object[] args, final RowMapper rowMapper)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		Object obj= null;
		try
		{
			obj = jdbcTemplate.queryForObject(sql, args, rowMapper);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForObject sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForObject Error: {}", ex.getMessage());
		}
		
		return obj;
		
	}
	
	protected Object queryForObject(final DataSource dataSource, final String sql, final Object[] args, final Class<T> mappedClass)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		Object obj= null;
		try
		{
			obj = jdbcTemplate.queryForObject(sql, args, new BeanPropertyRowMapper(mappedClass));
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForObject sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForObject Error: {}", ex.getMessage());
		}
		
		return obj;
		
	}
	
	protected String queryForString(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		String result = null;
		try 
		{
			result = (String)jdbcTemplate.queryForObject(sql, args, String.class);
		} catch (Exception ex) {
			LOG.error("BaseDao.queryForString sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForString Error: {}", ex.getMessage());
		}
		return result;	
	}
	
	protected long queryForLong(final DataSource dataSource, final String sql, final Object[] args)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		long obj = 0;
		try
		{
			obj = jdbcTemplate.queryForObject(sql, args, Long.class);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForLong sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForLong Error: {}", ex.getMessage());
		}
		
		return obj;
		
	}
	
	protected int queryForInt(final DataSource dataSource, final String sql, final Object[] args)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		int obj = 0;
		try
		{
			obj = jdbcTemplate.queryForObject(sql, args, Integer.class);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForInt sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForInt Error: {}", ex.getMessage());
		}
		
		return obj;
		
	}
	
	protected List queryForList(final DataSource dataSource, final String sql, final Object[] args, final RowMapper rowMapper)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list= null;
		try
		{
			list = jdbcTemplate.query(sql, args, rowMapper);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForList sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForList Error: {}", ex.getMessage());
			list= new ArrayList();
		}
		
		return list;
		
	}
	
	protected List queryForList(final DataSource dataSource, final String sql, final Object[] args, final  Class<T> mappedClass)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list= null;
		try
		{
			list = jdbcTemplate.query(sql, args, new BeanPropertyRowMapper(mappedClass));
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForList sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForList Error: {}", ex.getMessage());
			list= new ArrayList();
		}
		
		return list;
		
	}
	
	protected List queryForObjectList(final DataSource dataSource, final String sql, final Object[] args, final Class clazz)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list= null;
		try
		{
			list = jdbcTemplate.queryForList(sql, args, clazz);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForObjectList sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForObjectList Error: {}", ex.getMessage());
			list= new ArrayList();
		}
		
		return list;
	}
	
	protected List<Long> queryForLongList(final DataSource dataSource, final String sql, final Object[] args)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List<Long> list= null;
		try
		{
			list = jdbcTemplate.queryForList(sql, args, Long.class);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForLongList sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForLongList Error: {}", ex.getMessage());
			list= new ArrayList<Long>();
		}
		
		return list;
	}
	
	protected Page<?> queryForPage(final DataSource dataSource, Page<?> page, final String sql, final String countSql, final Object[] args, final Class<T> mappedClass) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list= null;
		int totalCount = 0;
		try
		{
			list = jdbcTemplate.query(sql, args, new BeanPropertyRowMapper(mappedClass));
			totalCount = jdbcTemplate.queryForObject(countSql, args, Integer.class);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForPage sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForPage Error: {}", ex.getMessage());
		}	
		page.setResult(list);
		page.setTotalCount(totalCount);

		return page;
	}
	
	protected Page<?> queryForPage(final DataSource dataSource, Page<?> page, final String sql, final String countSql, final Object[] args, final RowMapper rowMapper) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		List list= null;
		int totalCount = 0;
		try
		{
			list = jdbcTemplate.query(sql, args, rowMapper);
			totalCount = jdbcTemplate.queryForObject(countSql, args, Integer.class);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForPage sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForPage Error: {}", ex.getMessage());
		}	
		page.setResult(list);
		page.setTotalCount(totalCount);

		return page;
	}
	
	protected Map queryForMap(final DataSource dataSource, final String sql, final Object[] args)
	{
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		Map map = null;
		try
		{
			map = jdbcTemplate.queryForMap(sql, args);
		} catch(Exception ex) {
			LOG.error("BaseDao.queryForMap sql="+ fetchCombineSqlString(sql, args));
			LOG.error("BaseDao.queryForMap Error: {}", ex.getMessage());
			map = new HashMap();
		}
		
		return map;
		
	}
	
	protected BaseDto addForParam(final DataSource dataSource, final String sql, final BaseDto dto, final String[] params) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		jdbcTemplate.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection conn)
					throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql,
						Statement.RETURN_GENERATED_KEYS);
				for (int i=0; i<params.length; i++) {
					try {
						ps.setObject(i+1, PropertyUtils.getProperty(dto, params[i]));
					} catch (Exception e) {
						LOG.error("BaseDao.addForParam sql="+ sql);
						LOG.error("BaseDao.addForParam Error: {}", e.getMessage());
					}

				}
				return ps;
			}
		}, keyHolder);
		
		dto.setId(keyHolder.getKey().longValue());
		return dto;
	}
	
	protected BaseDto addForObject(final DataSource dataSource, final String sql, final BaseDto dto, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		jdbcTemplate.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection conn)
					throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql,
						Statement.RETURN_GENERATED_KEYS);
				for (int i=0; i<args.length; i++) {
					try {
						ps.setObject(i+1, args[i]);
					} catch (Exception e) {
						LOG.error("BaseDao.addForObject sql="+ fetchCombineSqlString(sql, args));
						LOG.error("BaseDao.addForObject Error: {}", e.getMessage());
					}

				}
				return ps;
			}
		}, keyHolder);
		
		dto.setId(keyHolder.getKey().longValue());
		return dto;
	}
	
	protected int updateForParam(final DataSource dataSource, final String sql, final BaseDto dto, final String[] params) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		int result = jdbcTemplate.update(sql,
				new PreparedStatementSetter() {
					public void setValues(PreparedStatement ps)
							throws SQLException {
						for (int i=0; i<params.length; i++) {
							try {
								ps.setObject(i+1, PropertyUtils.getProperty(dto, params[i]));
							} catch (Exception e) {
								LOG.error("BaseDao.updateForParam sql="+ sql);
								LOG.error("BaseDao.updateForParam Error: {}", e.getMessage());
							}

						}
					}
				});
		return result;
	}
	
	protected int updateForObject(final DataSource dataSource, final String sql, final Object[] args) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		return jdbcTemplate.update(sql, args);
	}	
}
