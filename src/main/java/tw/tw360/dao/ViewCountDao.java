package tw.tw360.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.ViewCount;
import tw.tw360.dto.Member;

/**
 * 會員資料
 * @version 1.0
 * */
@Repository
public class ViewCountDao extends BaseDao{
	
	@Resource(name = "dsOfferwallQry")
	private DataSource dsOfferwallQry;
	
	@Resource(name = "dsOfferwallUpd")
	private DataSource dsOfferwallUpd;
	
	@Autowired
	public ViewCountDao(@Qualifier("dsOfferwallUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public ViewCount add(final ViewCount br) {
		final String sql = 
				"INSERT INTO view_count(platform, create_time) values "+
						"(?,?)";
		
		return (ViewCount) addForObject(dsOfferwallUpd, sql, br, new Object[] { br.getPlatform(), br.getCreateTime() });
	}
	
	public ViewCount findById(long id) {
		
		return (ViewCount) queryForObject(dsOfferwallQry,"SELECT * FROM view_count where id = ?",new Object[]{id}, ViewCount.class);
	}
	
	public ViewCount findByPlatform(String platform) {
		
		return (ViewCount) queryForObject(dsOfferwallQry,"SELECT * FROM view_count where platform = ?",
						new Object[]{ platform }, ViewCount.class);
	}

	public int updateCount(String platform, int count) {
		final String sql = "update view_count set count=? where platform=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { count, platform });
	}
}
