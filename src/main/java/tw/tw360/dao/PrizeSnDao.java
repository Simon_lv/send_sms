package tw.tw360.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.PrizeSn;

/**
 * 會員資料
 * @version 1.0
 * */
@Repository
public class PrizeSnDao extends BaseDao{
	
	@Resource(name = "dsOfferwallQry")
	private DataSource dsOfferwallQry;
	
	@Resource(name = "dsOfferwallUpd")
	private DataSource dsOfferwallUpd;
	
	@Autowired
	public PrizeSnDao(@Qualifier("dsOfferwallUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public PrizeSn findById(int id) {
		
		return (PrizeSn) queryForObject(dsOfferwallQry,"SELECT * FROM prize_sn where status=1 and id = ?",new Object[]{id}, PrizeSn.class);
	}
	
	public PrizeSn getAvailableSn(int prizeId){
		String strSQL =  "select * from prize_sn where status=1 and prize_id = ? limit 1";
		
		return (PrizeSn) queryForObject(dsOfferwallQry, strSQL, new Object[] { prizeId }, PrizeSn.class);
	}
	
	public int updateStatus(long id, int status){
		String strSQL =  "update prize_sn set status=? where id = ?";
		
		return updateForObject(dsOfferwallUpd, strSQL, new Object[]{ status, id });
	}
	
	public List<Map<String, Object>> countByPrize()
	{
		String strSQL = "select CAST(prize_id as CHAR) pid, count(*) cnt from prize_sn where status=1 group by prize_id";
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		return jdbcTemplate.queryForList(strSQL);
	}
	
}
