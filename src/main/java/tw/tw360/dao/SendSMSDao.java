package tw.tw360.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.ZgActivity;

/**
 * 會員資料
 * @version 1.0
 * */
@Repository
public class SendSMSDao extends BaseDao{
	
	@Resource(name = "dsOfferwallQry")
	private DataSource dsOfferwallQry;
	
	@Resource(name = "dsOfferwallUpd")
	private DataSource dsOfferwallUpd;
	
	@Autowired
	public SendSMSDao(@Qualifier("dsOfferwallUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public int updateStatus(int id, int status) {
		final String sql = "update zg2_activity set status=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { status, id });
	}
	
	public List<ZgActivity> queryAll(String device)
	{
		String sql = "select * from zg2_activity where status=1";
		if (StringUtils.isNotBlank(device)) {
			sql = "select * from zg2_activity where status=1 and device=?";
			return queryForList(dsOfferwallQry, sql, new Object[] { device }, ZgActivity.class);
		}	
		return queryForList(dsOfferwallQry, sql, new Object[] { }, ZgActivity.class);
	}

}
