package tw.tw360.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.DailyLoginRecord;
import tw.tw360.dto.Member;
import tw.tw360.utils.PrintUtil;

/**
 * 會員資料
 * @version 1.0
 * */
@Repository
public class MemberDao extends BaseDao{
	
	@Resource(name = "dsOfferwallQry")
	private DataSource dsOfferwallQry;
	
	@Resource(name = "dsOfferwallUpd")
	private DataSource dsOfferwallUpd;
	
	@Autowired
	public MemberDao(@Qualifier("dsOfferwallUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public Page<Member> findAll(Page<Member> page,HashMap<String, String> map){
		StringBuffer strSQL =  new StringBuffer("select m.* from member as m where 1=1 ");
		StringBuffer pageSQL = new StringBuffer("select count(*) from member as m where 1=1 ");
		
		if(!StringUtils.isBlank(map.get("id"))) {
			strSQL.append(" and m.id = '"+map.get("id")+"'");
			pageSQL.append(" and m.id = '"+map.get("id")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("username"))) {
			strSQL.append(" and m.username = '"+map.get("username")+"'");
			pageSQL.append(" and m.username = '"+map.get("username")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("loginName"))) {
			strSQL.append(" and m.loginName = '"+map.get("loginName")+"'");
			pageSQL.append(" and m.loginName = '"+map.get("loginName")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("mobile"))) {
			strSQL.append(" and m.mobile = '"+map.get("mobile")+"'");
			pageSQL.append(" and m.mobile = '"+map.get("mobile")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("promoteCode"))) {
			strSQL.append(" and m.promote_code = '"+map.get("promoteCode")+"'");
			pageSQL.append(" and m.promote_code = '"+map.get("promoteCode")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("facebookEmail"))) {
			strSQL.append(" and m.facebook_email = '"+map.get("facebookEmail")+"'");
			pageSQL.append(" and m.facebook_email = '"+map.get("facebookEmail")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("bindPromoteCode"))) {
			strSQL.append(" and m.bind_promote_code = '"+map.get("bindPromoteCode")+"'");
			pageSQL.append(" and m.bind_promote_code = '"+map.get("bindPromoteCode")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("isVip")) && Integer.parseInt(map.get("isVip")) != -1){
			strSQL.append(" and m.is_vip = '"+map.get("isVip")+"'");
			pageSQL.append(" and m.is_vip = '"+map.get("isVip")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("status")) && Integer.parseInt(map.get("status")) != -1){
			strSQL.append(" and m.status = '"+map.get("status")+"'");
			pageSQL.append(" and m.status = '"+map.get("status")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("startTime"))){
			strSQL.append(" and m.create_time>='"+map.get("startTime")+"'");
			pageSQL.append(" and m.create_time>='"+map.get("startTime")+"'");
		}
		
		if(!StringUtils.isBlank(map.get("endTime"))){
			strSQL.append(" and m.create_time<='"+map.get("endTime")+"'");
			pageSQL.append(" and m.create_time<='"+map.get("endTime")+"'");
		}
		
		strSQL.append(" order by m.id desc limit "+page.getFirst()+","+page.getPageSize()+"");
		
		PrintUtil.outputContent("會員列表SQL:"+strSQL.toString());
		
	    page = queryForPage(dsOfferwallQry, page, strSQL.toString(), pageSQL.toString(), new Object[]{}, Member.class);
	    
	  return page;
	}

	public Member findByEmail(String email) {

		return (Member) queryForObject(dsOfferwallQry,"SELECT m.* FROM member as m WHERE email = ? and status < 3 limit 1",new Object[]{email}, Member.class);
	}
	
	public Member findByImei(String imei) {

		return (Member) queryForObject(dsOfferwallQry,"SELECT m.* FROM member as m WHERE device_imei = ? and status = 1 limit 1",new Object[]{imei}, Member.class);
	}
	
	public Member findById(Long id) {
		
		Member member = (Member) queryForObject(dsOfferwallQry,"SELECT m.* FROM member as m where m.id = ?",new Object[]{id}, Member.class);
		return member;
	}
	
	public int updateStatus(long id, int status) {
		final String sql = "update member set status=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { status, id });
	}
	
	public int updateIsPayLock(long id, int isPayLock) {
		final String sql = "update member set is_pay_lock=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { isPayLock, id });
	}
	
	
	public int updatePassword(long id, String password) {
		final String sql = "update member set password=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { password, id });
	}
	
	
	public int updateNameAndMobileAndEmail(String name,String mobile,String email,long id) {
		final String sql = "update member set name=?,mobile=?,email=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { name,mobile,email, id });
	}
	
	
	public int updateName(String name,long id) {
		final String sql = "update member set name=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { name, id });
	}
	
	public void update(final Member member) {
		final String sql = "update member set name=?,email=?,mobile=?,address=?,last_login_time=?,login_count=?,vcode=?,vcode_time=?,register_time=?,login_err_count=?,login_err_time=?,status=? where id=?";

		updateForObject(dsOfferwallUpd, sql, new Object[] { member.getName(), member.getEmail(), member.getMobile(), 
				member.getAddress(), member.getLastLoginTime(), member.getLoginCount(), member.getVcode(), member.getVcodeTime(), 
				member.getRegisterTime(), member.getLoginErrCount(), member.getLoginErrTime(), member.getStatus(), member.getId()});
	}
	
	public Member add(final Member member) {
		final String sql = 
				"INSERT INTO member(username,account,platform,password,email,mobile,create_time,status,android_id,device_imei,device_mac,vcode,vcode_time, recomm_code) values "+
						"(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		return (Member) addForObject(dsOfferwallUpd, sql, member, new Object[] { member.getUsername(), member.getAccount(), member.getPlatform(), member.getPassword(), member.getEmail(),
			member.getMobile(), member.getCreateTime(),  member.getStatus(),member.getAndroidId(),member.getDeviceImei(),member.getDeviceMac(), member.getVcode(), member.getVcodeTime(), member.getRecommCode() });
	}
	
	
	public Member findByUsername(String username) {
		System.out.println("in query=====findByUsername===="+username);
		return (Member) queryForObject(dsOfferwallQry,"SELECT * FROM member where username = ?",new Object[]{username}, Member.class);
	}
	
	public int updateEmail(long id, String email) {
		final String sql = "update member set email=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { email, id });
	}
	
	public int updateVcode(long id, String vcode) {
		final String sql = "update member set vcode=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { vcode, id });
	}
	
	public int updateBonus(long id, int bonus) {
		final String sql = "update member set bonus=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { bonus, id });
	}
	
	public int updateImei(long id, String imei) {
		final String sql = "update member set device_imei=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { imei, id });
	}
	
	public int updateVcodeAndVcodeTime(long id, String vcode, Timestamp vcodeTime) {
		final String sql = "update member set vcode=?,vcode_time=? where id=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { vcode, vcodeTime, id });
	}
	
	public DailyLoginRecord findDailyLoginRecordByMemberIdAndCurrentDate(String memberId, String currentDate) {
		return (DailyLoginRecord) queryForObject(dsOfferwallQry,"SELECT * FROM daily_login_record WHERE member_id = ? and login_current_date = ?",new Object[]{memberId, currentDate}, DailyLoginRecord.class);
	}
	
	public DailyLoginRecord addDailyLoginRecord(final DailyLoginRecord dailyLoginRecord) {
		final String sql = 
				"INSERT INTO daily_login_record(member_id, login_current_date, platform, login_count) values "+
						"(?,?,?,?)";
		return (DailyLoginRecord) addForObject(dsOfferwallUpd, sql, dailyLoginRecord, new Object[] {dailyLoginRecord.getMemberId(), dailyLoginRecord.getLoginCurrentDate(), dailyLoginRecord.getPlatform(), dailyLoginRecord.getLoginCount()});
	}
	
	public int updateDailyLoginRecordLoginCount(final DailyLoginRecord dailyLoginRecord) {
		final String sql = 
				"UPDATE daily_login_record set login_count = ? where member_id = ? and login_current_date = ? and platform = ?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] {dailyLoginRecord.getLoginCount(),dailyLoginRecord.getMemberId(), dailyLoginRecord.getLoginCurrentDate(), dailyLoginRecord.getPlatform()});
	} 
	
	public int updateLoginErr(long id, int errCount, Timestamp errTime) {
		final String sql = 
				"UPDATE member set login_err_count = ?, login_err_time = ? where id = ?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] {errCount, errTime, id});
	} 
	
	/*
	 * member-list.jsp的查詢功能
	 */
	public Page<Map<String, Object>> findByIdCtLt(Page<Map<String, Object>> page, String account, String startCreateTime, String endCreateTime, String startLastLoginTime, String endLastLoginTime, int pageNo, int rec) {
		String condition;
		if(account != null && !account.trim().equals("")) {
			condition = "member.account = "+account+" and ";
		} else {
			condition = "";
		}
		
		if(endCreateTime.equals("")) {
			endCreateTime = "9999-12-31 23:59:59";
		}
		
		if(endLastLoginTime.equals("")) {
			endLastLoginTime = "9999-12-31 23:59:59";
		}
		
		String strSQL = "select " +
				"member.id id, "+
				"member.account account, "+
				"member.email email, "+
				"DATE(member.create_time) createTime, "+
				"DATE(member.last_login_time) lastLoginTime, "+
				"lower(member.status) status, "+
				"(select sum(bonus) from bonus_record where member.id=bonus_record.member_id) sumBrBonus, "+
				"member.bonus bonus, "+
				"member.device_imei deviceIMEI "+
				"from member where "+
				condition+
				"create_time >= ? and create_time <= ? and last_login_time >= ? and last_login_time <= ? "+
				"order by create_time desc limit ?, ? ";
		String pageSQL = "select count(*) from member where "+
				condition+
				"create_time >= ? and create_time <= ? and last_login_time >= ? and last_login_time <= ? ";
		
		
		int start = (pageNo - 1) * rec;
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		List<Map<String, Object>> list = jdbcTemplate.queryForList(strSQL, startCreateTime, endCreateTime, startLastLoginTime, endLastLoginTime, start, rec);
		int totalCount = jdbcTemplate.queryForObject(pageSQL, new Object[] { startCreateTime, endCreateTime, startLastLoginTime, endLastLoginTime }, Integer.class);
		page.setResult(list);
		page.setTotalCount(totalCount);
		return page;
	}
	
	/*
	 * 加入黑名單
	 */
	public void addBlocklist(long id) {
		String sql = "update member set `status` = 2 where id = ?";
		
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallUpd);
		jdbcTemplate.update(sql, id);
	}

	/*
	 * member-list.jsp的excel匯出
	 */
	public List<Map<String, Object>> findByIdCtLt(String account, String startCreateTime, String endCreateTime, String startLastLoginTime, String endLastLoginTime) {
		String condition;
		if(account != null && !account.trim().equals("")) {
			condition = "member.account = "+account+" and ";
		} else {
			condition = "";
		}
		
		if(endCreateTime.equals("")) {
			endCreateTime = "9999-12-31 23:59:59";
		}
		
		if(endLastLoginTime.equals("")) {
			endLastLoginTime = "9999-12-31 23:59:59";
		}
		
		String strSQL = "select " +
				"member.id id, "+
				"member.account account, "+
				"member.email email, "+
				"DATE(member.create_time) createTime, "+
				"DATE(member.last_login_time) lastLoginTime, "+
				"lower(member.status) status, "+
				"(select sum(bonus) from bonus_record where member.id=bonus_record.member_id) sumBrBonus, "+
				"member.bonus bonus, "+
				"member.device_imei deviceIMEI "+
				"from member where "+
				condition+
				"create_time >= ? and create_time <= ? and last_login_time >= ? and last_login_time <= ? "+
				"order by create_time desc";
		
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		return jdbcTemplate.queryForList(strSQL, startCreateTime, endCreateTime, startLastLoginTime, endLastLoginTime);
	}

	public boolean existRecommdCode(String code) {
		String sql = "SELECT COUNT(*) FROM member WHERE recomm_code = ?";
		return queryForInt(dsOfferwallQry, sql, new Object[]{code}) > 0;
	}

	public int countFriendCode(String code) {
		String sql = "SELECT COUNT(*) FROM member WHERE friend_code = ?";
		return queryForInt(dsOfferwallQry, sql, new Object[]{code});
	}

	public int invite(Member member, String code) {
		final String sql = "UPDATE member SET friend_code=?, bonus=bonus+15 WHERE id=? AND friend_code IS NULL";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { code,member.getId() });
	}

	public int updateBonusByRecommCode(String code) {
		final String sql = "UPDATE member SET bonus=bonus+10 WHERE recomm_code=?";
		return updateForObject(dsOfferwallUpd, sql, new Object[] { code });
	}
}
