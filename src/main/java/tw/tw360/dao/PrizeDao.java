package tw.tw360.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Prize;

/**
 * 會員資料
 * @version 1.0
 * */
@Repository
public class PrizeDao extends BaseDao{
	
	@Resource(name = "dsOfferwallQry")
	private DataSource dsOfferwallQry;
	
	@Resource(name = "dsOfferwallUpd")
	private DataSource dsOfferwallUpd;
	
	@Autowired
	public PrizeDao(@Qualifier("dsOfferwallUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public Prize findById(int id) {
		
		return (Prize) queryForObject(dsOfferwallQry,"SELECT * FROM prize where status=1 and id = ?",new Object[]{id}, Prize.class);
	}
	
	public Page<Prize> findAll(Page<Prize> page,HashMap<String, String> map, int pageNo, int rec){
		StringBuffer strSQL =  new StringBuffer("select * from prize where status=1 ");
		StringBuffer pageSQL = new StringBuffer("select count(*) from prize where status=1 ");
		strSQL.append("and (start_time is null or start_time <= NOW()) ");
		strSQL.append("and (end_time is null or end_time >= NOW()) ");
		pageSQL.append("and (start_time is null or start_time <= NOW()) ");
		pageSQL.append("and (end_time is null or end_time >= NOW()) ");
		if(!StringUtils.isBlank(map.get("platform"))) {
			strSQL.append("and platform = '"+map.get("platform")+"' ");
			pageSQL.append("and platform = '"+map.get("platform")+"' ");
		}
		strSQL.append("order by order_index, create_time desc limit ?, ?");
		int start = (pageNo - 1) * rec;
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		List list = jdbcTemplate.query(strSQL.toString(), new Object[] {start, rec}, new BeanPropertyRowMapper(Prize.class));
		int totalCount = jdbcTemplate.queryForObject(pageSQL.toString(), new Object[] { }, Integer.class);
		page.setResult(list);
		page.setTotalCount(totalCount);
	    return page;
	}
	
	public Page<Prize> findAllByMemberId(Page<Prize> page,HashMap<String, String> map, int pageNo, int rec, long memberId){
		StringBuffer strSQL =  new StringBuffer("select * from prize where status=1 ");
		StringBuffer pageSQL = new StringBuffer("select count(*) from prize where status=1 ");
		strSQL.append("and (start_time is null or start_time <= NOW()) ");
		strSQL.append("and (end_time is null or end_time >= NOW()) ");
		pageSQL.append("and (start_time is null or start_time <= NOW()) ");
		pageSQL.append("and (end_time is null or end_time >= NOW()) ");
		
		//member限制
		strSQL.append(" and ( ");
		strSQL.append("    limit_type = 0 ");
		strSQL.append("    OR (limit_type = 1 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" AND DATE(create_time) = DATE(NOW()) ) ) ");
		strSQL.append("    OR (limit_type = 2 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" ) ) ");
		strSQL.append(") ");
		
		pageSQL.append(" and ( ");
		pageSQL.append("    limit_type = 0 ");
		pageSQL.append("    OR (limit_type = 1 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" AND DATE(create_time) = DATE(NOW()) ) ) ");
		pageSQL.append("    OR (limit_type = 2 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" ) ) ");
		pageSQL.append(") ");
		
		if(!StringUtils.isBlank(map.get("platform"))) {
			strSQL.append("and platform = '"+map.get("platform")+"' ");
			pageSQL.append("and platform = '"+map.get("platform")+"' ");
		}
		strSQL.append("order by order_index, create_time desc limit ?, ?");
		int start = (pageNo - 1) * rec;
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		
		List list = jdbcTemplate.query(strSQL.toString(), new Object[] {start, rec}, new BeanPropertyRowMapper(Prize.class));
		int totalCount = jdbcTemplate.queryForObject(pageSQL.toString(), new Object[] { }, Integer.class);
		page.setResult(list);
		page.setTotalCount(totalCount);
		
	    return page;
	}
	
	
	
	public Page<Map<String, Object>> findAllForThird(Page<Map<String, Object>> page,HashMap<String, String> map, int pageNo, int rec){
		StringBuffer strSQL =  new StringBuffer("select order_index AS `order`, CAST(type as SIGNED) as type, id, img_url AS imgUrl, NAME, description, `usage`, bonus, start_time AS startTime, end_time AS endTime from prize where status=1 ");
		StringBuffer pageSQL = new StringBuffer("select count(*) from prize where status=1 ");
		strSQL.append("and (start_time is null or start_time <= NOW()) ");
		strSQL.append("and (end_time is null or end_time >= NOW()) ");
		pageSQL.append("and (start_time is null or start_time <= NOW()) ");
		pageSQL.append("and (end_time is null or end_time >= NOW()) ");
		if(!StringUtils.isBlank(map.get("platform"))) {
			strSQL.append("and platform = '"+map.get("platform")+"' ");
			pageSQL.append("and platform = '"+map.get("platform")+"' ");
		}
		strSQL.append("order by order_index, create_time desc limit ?, ?");
		int start = (pageNo - 1) * rec;
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		List list = jdbcTemplate.queryForList(strSQL.toString(), new Object[] {start, rec});
		int totalCount = jdbcTemplate.queryForObject(pageSQL.toString(), new Object[] { }, Integer.class);
		page.setResult(list);
		page.setTotalCount(totalCount);
	    return page;
	}
	
	public boolean checkLimitType1(Long prId, Long memberId) {
		String sql = "SELECT COUNT(*) FROM exchange_record WHERE member_id = ? AND prize_id = ? AND DATE(create_time) = DATE(NOW()) ;";
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		List<Integer> list = jdbcTemplate.queryForList(sql.toString(), new Object[] {memberId, prId}, Integer.class);
		
		return list.get(0) == 0;
	}
	
	public boolean checkLimitType2(Long prId, Long memberId) {
		String sql = "SELECT COUNT(*) FROM exchange_record WHERE member_id = ? AND prize_id = ?";
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsOfferwallQry);
		List<Integer> list = jdbcTemplate.queryForList(sql.toString(), new Object[] {memberId, prId}, Integer.class);
		
		return list.get(0) == 0;
	}
	
public Page<Prize> findAllByMemberIdOrType(Page<Prize> page,HashMap<String, String> map, int pageNo, int rec, long memberId,String type){
        
        StringBuffer strSQL =  new StringBuffer("select * from prize where status=1 ");
        StringBuffer pageSQL = new StringBuffer("select count(*) from prize where status=1 ");
        strSQL.append("and (start_time is null or start_time <= NOW()) ");
        strSQL.append("and (end_time is null or end_time >= NOW()) ");
        pageSQL.append("and (start_time is null or start_time <= NOW()) ");
        pageSQL.append("and (end_time is null or end_time >= NOW()) ");
        
        //member限制
        strSQL.append(" and ( ");
        strSQL.append("    limit_type = 0 ");
        strSQL.append("    OR (limit_type = 1 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" AND DATE(create_time) = DATE(NOW()) ) ) ");
        strSQL.append("    OR (limit_type = 2 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" ) ) ");
        strSQL.append(") ");
        
        pageSQL.append(" and ( ");
        pageSQL.append("    limit_type = 0 ");
        pageSQL.append("    OR (limit_type = 1 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" AND DATE(create_time) = DATE(NOW()) ) ) ");
        pageSQL.append("    OR (limit_type = 2 AND id NOT IN ( SELECT prize_id FROM exchange_record WHERE member_id = "+memberId+" ) ) ");
        pageSQL.append(") ");
        
        if(!StringUtils.isBlank(map.get("platform"))) {
            strSQL.append("and platform = '"+map.get("platform")+"' ");
            pageSQL.append("and platform = '"+map.get("platform")+"' ");
        }
        System.out.println("type:"+type);
        if(!StringUtils.isBlank(type)) {
            strSQL.append("and type "+( type=="3" ? "":"not" )+" in(3) ");
            pageSQL.append("and type "+( type=="3" ? "":"not" )+" in(3) ");
        }
        strSQL.append(" order by order_index, create_time desc limit ?, ?");
        int start = (pageNo - 1) * rec;
        JdbcTemplate jdbcTemplate = getJdbcTemplate();
        jdbcTemplate.setDataSource(dsOfferwallQry);
        
        System.out.println("prize sql:"+strSQL.toString());
        List list = jdbcTemplate.query(strSQL.toString(), new Object[] {start, rec}, new BeanPropertyRowMapper(Prize.class));
        int totalCount = jdbcTemplate.queryForObject(pageSQL.toString(), new Object[] { }, Integer.class);
        page.setResult(list);
        page.setTotalCount(totalCount);
        
        return page;
    }
	
	public Page<Prize> findAllByType(Page<Prize> page,HashMap<String, String> map, int pageNo, int rec ,String type){
        StringBuffer strSQL =  new StringBuffer("select * from prize where status=1 ");
        StringBuffer pageSQL = new StringBuffer("select count(*) from prize where status=1 ");
        strSQL.append("and (start_time is null or start_time <= NOW()) ");
        strSQL.append("and (end_time is null or end_time >= NOW()) ");
        pageSQL.append("and (start_time is null or start_time <= NOW()) ");
        pageSQL.append("and (end_time is null or end_time >= NOW()) ");
        if(!StringUtils.isBlank(map.get("platform"))) {
            strSQL.append("and platform = '"+map.get("platform")+"' ");
            pageSQL.append("and platform = '"+map.get("platform")+"' ");
        }
        if(!StringUtils.isBlank(type)) {
            strSQL.append("and type "+( type=="3" ? "":"not" )+" in(3) ");
            pageSQL.append("and type "+( type=="3" ? "":"not" )+" in(3) ");
        }
        strSQL.append("order by order_index, create_time desc limit ?, ?");
        int start = (pageNo - 1) * rec;
        JdbcTemplate jdbcTemplate = getJdbcTemplate();
        jdbcTemplate.setDataSource(dsOfferwallQry);
        System.out.println("prize sql:"+strSQL.toString());
        List list = jdbcTemplate.query(strSQL.toString(), new Object[] {start, rec}, new BeanPropertyRowMapper(Prize.class));
        int totalCount = jdbcTemplate.queryForObject(pageSQL.toString(), new Object[] { }, Integer.class);
        page.setResult(list);
        page.setTotalCount(totalCount);
        return page;
    }
}
