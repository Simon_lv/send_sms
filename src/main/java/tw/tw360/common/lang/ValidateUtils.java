package tw.tw360.common.lang;

import org.apache.commons.lang.StringUtils;

public class ValidateUtils {
	
	public static boolean checkMobile(String mobile, boolean canEmpty) {
		if(canEmpty && StringUtils.isBlank(mobile)) {
			return true;
		}
		if(!StringUtils.isBlank(mobile) && mobile.matches("^09[0-9]{8}$")) {
			return true;
		}
		return false;
	}
	
	public static boolean checkEmail(String email, boolean canEmpty) {
		if(canEmpty && StringUtils.isBlank(email)) {
			return true;
		}
		if(!StringUtils.isBlank(email) && email.toLowerCase().matches(
		        "^[_a-z0-9-]+([.][_a-z0-9-]+)*@[a-z0-9-]+([.][a-z0-9-]+)*$")) 
		{
			return true;
		}
		return false;
	}
	
	public static void main(String[] arg) {
		System.out.println(ValidateUtils.checkEmail("Jason.duck@gmail.com", true));
	}
}
