
package tw.tw360.common.mail;

import org.apache.log4j.Logger;

import EDU.oswego.cs.dl.util.concurrent.PooledExecutor;

/**
 * @author jim
 */
public class Executor
{
	private static Logger logger = Logger.getLogger(Executor.class);
	private static PooledExecutor executor = new PooledExecutor(5);
	
	static {
		executor.setMinimumPoolSize(2);
		executor.setMaximumPoolSize(10);
		executor.setKeepAliveTime(1000 * 60 * 10);
	}
	
	public static void execute(Runnable runnable) {
		try {
			executor.execute(runnable);
		}
		catch (Exception e) {
			logger.error("Exception while running task: " + e, e);
		}
	}
}
