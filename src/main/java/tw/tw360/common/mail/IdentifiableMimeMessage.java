package tw.tw360.common.mail;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

/**
 * @author jim
 * @version 
 */
public class IdentifiableMimeMessage extends MimeMessage
{
	private String messageId;
	
	public IdentifiableMimeMessage(Session session)
	{
		super(session);
	}
	
	/**
	 * Sets the Message-ID header for this message
	 * @param messageId the Message-ID
	 */
	public void setMessageId(String messageId)
	{
		this.messageId = messageId;
	}
	
	/**
	 * @see javax.mail.internet.MimeMessage#updateMessageID()
	 */
	protected void updateMessageID() throws MessagingException 
	{
		if (this.messageId != null) {
			this.addHeader("Message-ID", this.messageId);
		}
		else {
			super.updateMessageID();
		}
	}
}
