package tw.tw360.common.mail;

/**
 * @author jim
 */
public class EmailSenderTask implements Runnable
{
	private final Spammer spammer;

	public EmailSenderTask(Spammer spammer)
	{
		this.spammer = spammer;
	}

	/**
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		this.spammer.dispatchMessages();
	}
}
