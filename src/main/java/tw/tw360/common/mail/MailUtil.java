package tw.tw360.common.mail;

import java.io.UnsupportedEncodingException;
import java.net.PasswordAuthentication;
import java.security.Security;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailUtil {

	
	public static void sendGmail(String toEmailAddress,String emailSubject,String emailText) throws AddressException, MessagingException
	{
		String fromEmail = "service@8888play.com";
		String password = "";
		sendGmail(fromEmail,password,toEmailAddress,emailSubject,emailText);
	}
	
	 
	public static void sendGmail(String fromEmail,final String password,String toEmailAddress,String emailSubject,String emailText) throws AddressException, MessagingException
	{
		
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		   final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		   // Get a Properties object
		   Properties props = System.getProperties();
		   props.setProperty("mail.transport.protocol", "smtp");
		   props.setProperty("mail.smtp.host", "127.0.0.1");
		   //props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		   //props.setProperty("mail.smtp.socketFactory.fallback", "false");
		   props.setProperty("mail.smtp.port", "25");
		   //props.setProperty("mail.smtp.socketFactory.port", "465");
		   props.put("mail.smtp.auth", "true");
		   final String username = ""; //fromEmail
	
		   
	
		   Session session = Session.getDefaultInstance(props, new 
				   
				   Authenticator()
		   {
		       protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
		           return new javax.mail.PasswordAuthentication(username, password);
		       }}
		   );
		  

		   
		        // -- Create a new message --
		   Message msg = new MimeMessage(session);
		
		   // -- Set the FROM and TO fields --
		   msg.setFrom(new InternetAddress(fromEmail));
		   msg.setRecipients(Message.RecipientType.TO,
		     InternetAddress.parse(toEmailAddress,false));
		   msg.setSubject(emailSubject);
		  // msg.setText(emailText);
		   msg.setContent(emailText, "text/html;charset=utf-8");
		   msg.setSentDate(new Date());
		   Transport.send(msg);
	}
}
