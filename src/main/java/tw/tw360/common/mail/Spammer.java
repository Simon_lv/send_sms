package tw.tw360.common.mail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import freemarker.template.SimpleHash;
import freemarker.template.Template;

/**
 * Dispatch emails to the world. 
 * 
 * @author jim
 */
public class Spammer
{
	private static final Logger logger = Logger.getLogger(Spammer.class);

	
	private static final int MESSAGE_HTML = 0;
	private static final int MESSAGE_TEXT = 1;
	private static final boolean IS_SSL = true;
	private static final String SMTP_HOST = "smtp.gmail.com";
	private static final String SMTP_PORT = "465";
	private static final String SMTP_LOCAL_HOST = "";
	private static final boolean SMTP_AUTH = true;
	private static final boolean SMTP_SSL = true;
	private static final int SMTP_DELAY = 2000;
	private static final String PROJECT_NAME = "360台灣-手機助手應用開放平台";
	private static final String MAIL_SENDER ="cs@360tw.tw";
	private static final String MAIL_PASSWORD = "hopejoy360";
	private static final String FROM = "360台灣-手機助手應用開放平台";
	private static final String CHARSET = "UTF-8";
	private static final String MESSAGE_FORMAT = "text";
	private static int messageFormat;
	private Session session;
	private String username;
	private String password;
	
	private Properties mailProps = new Properties();
	private MimeMessage message;
	private String sendToEmail;
	private String messageId;
	private String inReplyTo;
	private boolean needCustomization;
	//private SimpleHash templateParams;
	private Template template;
	
	protected Spammer() throws Exception
	{
		boolean ssl = IS_SSL;
		
		String hostProperty = "";
		String portProperty = "";
		String authProperty = "";
		String localhostProperty = "";
		
		mailProps.put(hostProperty, SMTP_HOST);
		mailProps.put(portProperty, SMTP_PORT);

		String localhost = SMTP_LOCAL_HOST;
		
		if (!StringUtils.isEmpty(localhost)) {
			mailProps.put(localhostProperty, localhost);
		}
		
		mailProps.put("mail.mime.address.strict", "false");
		mailProps.put("mail.mime.charset", CHARSET);
		mailProps.put(authProperty, SMTP_AUTH);

		username = MAIL_SENDER;
		password = MAIL_PASSWORD;

		messageFormat = MESSAGE_HTML;//HTML

		this.session = Session.getInstance(mailProps);
	}

	/**
	 * 指定member發送email
	 * @return
	 */
	public boolean dispatchMessages()
	{
        try
        {
            int sendDelay = SMTP_DELAY;
            
			if (SMTP_AUTH) {
                if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
                	boolean ssl = SMTP_SSL;
                	
                    Transport transport = this.session.getTransport(ssl ? "smtps" : "smtp");
                    
                    try {
	                    String host = SMTP_HOST;
	                    transport.connect(host, username, password);
	                    
	                    if (transport.isConnected()) {
		                        	Address address = new InternetAddress(sendToEmail);
		                        	
		                        	logger.debug("Sending mail to: " + sendToEmail);
		                        	
		                        	this.message.setRecipient(Message.RecipientType.TO, address);
		                        	transport.sendMessage(this.message, new Address[] { address });
		                        	
		                        	if (sendDelay > 0) {
			                        	try {
			                            	Thread.sleep(sendDelay);
			                            } 
			                        	catch (InterruptedException ie) {
			                            	logger.error("Error while Thread.sleep." + ie, ie);
			                            }
		                        	}
	                    	}
	                        
                    }
                    catch (Exception e) {
                    	//throw new Exception(e);
                    }
                    finally {
                    	try { transport.close(); } catch (Exception e) {}
                    }
                }
            }
            else {
                	Address address = new InternetAddress(sendToEmail);
                	logger.debug("Sending mail to: " + sendToEmail);
                	this.message.setRecipient(Message.RecipientType.TO,address);
                    Transport.send(this.message, new Address[] { address });
                    
                    if (sendDelay > 0) {
	                    try {
	                    	Thread.sleep(sendDelay);
	                    } catch (InterruptedException ie) {
	                    	logger.error("Error while Thread.sleep." + ie, ie);
	                    }
                    }
            	}
        }
        catch (MessagingException e) {
            logger.error("Error while dispatching the message." + e, e);
        }

        return true;
	}

	
	
	/**
	 * Prepares the mail message for sending.
	 * 
	 * @param subject the subject of the email
	 * @param messageFile the path to the mail message template
	 * @throws MailException
	 */
	protected void prepareMessage(String subject, String mailSubject)
	{
		if (this.messageId != null) {
			this.message = new IdentifiableMimeMessage(session);
			((IdentifiableMimeMessage)this.message).setMessageId(this.messageId);
		}
		else {
			this.message = new MimeMessage(session);
		}
		
		//this.templateParams.put("projectName", PROJECT_NAME);

		try {
			this.message.setSentDate(new Date());
			this.message.setFrom(new InternetAddress(MAIL_SENDER,FROM));
			this.message.setSubject(subject, CHARSET);
			
			if (this.inReplyTo != null) {
				this.message.addHeader("In-Reply-To", this.inReplyTo);
			}
			this.message.setText(mailSubject);

		}
		catch (Exception e) {
			logger.error("Error while dispatching the message." + e, e);
		}
	}
	
	/**
	 * Set the text contents of the email we're sending
	 * @param text the text to set
	 * @throws MessagingException
	 */
	private void defineMessageText(String text) throws MessagingException
	{
		
		if (messageFormat == MESSAGE_HTML) {
			this.message.setContent(text.replaceAll("\n", "<br />"), "text/html; charset=" + CHARSET);
		}
		else {
			this.message.setText(text);
		}
	}
	


	/**
	 * Merge the template data, creating the final content.
	 * This method should only be called after {@link #createTemplate(String)}
	 * and {@link #setTemplateParams(SimpleHash)}
	 * 
	 * @return the generated content
	 * @throws Exception
	 */
//	protected String processTemplate() throws Exception
//	{
//		StringWriter writer = new StringWriter();
//		this.template.process(this.templateParams, writer);
//		return writer.toString();
//	}
	
	/**
	 * Set the parameters for the template being processed
	 * @param params the parameters to the template
	 */
//	protected void setTemplateParams(SimpleHash params)
//	{
//		this.templateParams = params;
//	}
	
	/**
	 * Check if we have to send customized emails
	 * @return true if there is a need for customized emails
	 */
	private boolean isCustomizationNeeded()
	{
		boolean need = false;
		return need;
	}
	
	protected void setMessageId(String messageId)
	{
		this.messageId = messageId;
	}
	
	protected void setInReplyTo(String inReplyTo)
	{
		this.inReplyTo = inReplyTo;
	}
	
	
	protected void setSendToEmail(String sendToEmail)
	{
		this.sendToEmail = sendToEmail;
	}

}
