package tw.tw360.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.MemberDao;
import tw.tw360.dto.DailyLoginRecord;
import tw.tw360.dto.Member;

import com.google.code.ssm.api.InvalidateSingleCache;
import com.google.code.ssm.api.ParameterValueKeyProvider;



/**
 * 會員資料Mannaer
 * @version 1.0
 * */
@Service
@Transactional
public class MemberManager {
	
	@Autowired
    private MemberDao memberDao;
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	
	private static final String[] TITLE = {
		"會員ID", "帳號", "Email", "註冊時間", "最後登入時間", "帳號狀態（正常/停用）", "累計獲得積分", "剩餘積分", 
		"IMEI"
	};
	
	private static final String[] TITLE_KEY = {
		"id", "account", "email", "createTime", "lastLoginTime", "status", "sumBrBonus", "bonus", 
		"deviceIMEI"
	};
	
	/**
	 * 查詢所有會員資料
	 * */
	public Page<Member> findAll(Page<Member> page,HashMap<String, String> map){
		return memberDao.findAll(page, map);
	}
	
	/**
	 * email查詢會員資料
	 * */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Member findByEmail(String email) {
		return memberDao.findByEmail(email);
	} 
	
	/**
	 * imei查詢會員資料
	 * */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Member findByImei(String imei) {
		return memberDao.findByImei(imei);
	} 
	
	/**
	 * id查詢會員資料
	 * */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
//	@ReadThroughSingleCache(namespace = "MemberManager.byId", expiration = 604800)
	public Member findById(@ParameterValueKeyProvider Long id) {
		return memberDao.findById(id);
	}
	
	@InvalidateSingleCache(namespace = "MemberManager.byId")
	public void invaldateCacheById(@ParameterValueKeyProvider final Long id) {
		
	}
	
	/**
	 * 保存會員資料
	 * */
	public Member add(final Member member) {
		Member m = null;
		
		while(true) {
			try {
				String recommCode = RandomStringUtils.random(10, "ABCDEFGHIJKLMN0123456789");
				if(recommCode.length() != 10) {
					continue;
				}
				member.setRecommCode(recommCode);
				m = memberDao.add(member);
				
				if(m != null) {
					break;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		 return m;
	}
	
	public void update(final Member member) {
		memberDao.update(member);
	}
	
	/**
	 * 更改會員狀態
	 * */
	public void changeStatusById(long id, int status) {
		memberDao.updateStatus(id, status);
	}
	
	public void changeIsPayLockById(long id, int isPayLock) {
		memberDao.updateIsPayLock(id, isPayLock);
	}
	
	@InvalidateSingleCache(namespace = "MemberManager.byUsername")
	public void invaldateCacheByUsername(@ParameterValueKeyProvider final String username) {
		
	}
	
	public int changePasswordById(long id, String changePassword) {
		return memberDao.updatePassword(id, changePassword);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
//	@ReadThroughSingleCache(namespace = "MemberManager.byUsername", expiration = 604800)
	public Member findByUsername(@ParameterValueKeyProvider final String username) {
		 return memberDao.findByUsername(username);
	}
	
	public int updateNameAndMobileAndEmail(String name,String mobile,String email,long id)
	{
		return memberDao.updateNameAndMobileAndEmail(name, mobile, email, id);
	}
	
	
	public int updateName(String name,long id) {
		return memberDao.updateName(name, id);
	}
	
	public int updateEmail(long id, String email) {
		int result = memberDao.updateEmail(id, email);
		return result;
	}
	
	public int updateVcode(long id, String vcode) {
		int result = memberDao.updateVcode(id, vcode);
		return result;
	}
	
	public int updateBonus(long id, int bonus) {
		int result = memberDao.updateBonus(id, bonus);
		return result;
	}
	
	public int updateImei(long id, String imei) {
		int result = memberDao.updateImei(id, imei);
		return result;
	}
	public int updateVcodeAndVcodeTime(long id, String vcode, Timestamp timestamp) {
		int result = memberDao.updateVcodeAndVcodeTime( id,  vcode,  timestamp) ;
		return result;
	}
	
	public void upsertDailyLoginRecord(final Member member) {
		String currentDate = sdf.format(new Date());
		DailyLoginRecord dailyLoginRecord = memberDao.findDailyLoginRecordByMemberIdAndCurrentDate(member.getId().toString(), currentDate);
		if (null == dailyLoginRecord) {
			String username = member.getUsername();
			// 切開username取得平台，以@切開失敗的話存空白
			int indexOf = username.indexOf("@");
			String platform = "";
			if (indexOf > 0 ) {
				platform = username.substring(indexOf + 1);
			}
			dailyLoginRecord = new DailyLoginRecord();
			dailyLoginRecord.setLoginCount(1);
			dailyLoginRecord.setLoginCurrentDate(currentDate);
			dailyLoginRecord.setMemberId(member.getId());
			dailyLoginRecord.setPlatform(platform);
			memberDao.addDailyLoginRecord(dailyLoginRecord);
		} else {
			int loginCount = dailyLoginRecord.getLoginCount();
			dailyLoginRecord.setLoginCount(++loginCount);
			memberDao.updateDailyLoginRecordLoginCount(dailyLoginRecord);
		}
	}
	
	public int updateLoginErr(long id, int errCount, Timestamp errTime) {
		return memberDao.updateLoginErr(id, errCount, errTime);
	}

	/*
	 * member-list.jsp的查詢功能
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<Map<String, Object>> findByIdCtLt(Page<Map<String, Object>> page, String account, String startCreateTime, String endCreateTime, String startLastLoginTime, String endLastLoginTime, int pageNo, int rec) {
		return memberDao.findByIdCtLt(page, account, startCreateTime, endCreateTime, startLastLoginTime, endLastLoginTime, pageNo, rec);
	}
	
	/*
	 * 加入黑名單
	 */
	public void addBlocklist(long id) {
		memberDao.addBlocklist(id);
	}

	/*
	 * member-list.jsp的excel匯出
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Map<String, Object>> findByIdCtLt(String account, String startCreateTime, String endCreateTime, String startLastLoginTime, String endLastLoginTime) {
		return memberDao.findByIdCtLt(account, startCreateTime, endCreateTime, startLastLoginTime, endLastLoginTime);
	}

	public boolean existRecommdCode(String code) {
		return memberDao.existRecommdCode(code);
	}

	public int countFriendCode(String code) {
		return memberDao.countFriendCode(code);
	}

	public String invite(Member member, String code) {
		int result = memberDao.invite(member, code);
		if(member.getRecommCode().equals(code)) {
			return "不可以邀請自己";
		} else if(result > 0) {
			memberDao.updateBonusByRecommCode(code);
			return "邀請成功";
		} else {
			return "已經邀請過好友";
		}
	}
}
