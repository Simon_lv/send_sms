package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.SendSMSDao;
import tw.tw360.dto.ZgActivity;

/**
 * 簡訊紀錄
 * @version 1.0
 * */
@Service
@Transactional
public class SendSMSManager {
	@Autowired
    private SendSMSDao smsRecordDao;
	
	/**
	 * 用簡訊ID找簡訊
	 * */
	public int updateStatus(int id, int status) {
		return smsRecordDao.updateStatus(id, status);
	}
	
	public List<ZgActivity> queryAll(String device){
		return smsRecordDao.queryAll(device);
	}
	
}
