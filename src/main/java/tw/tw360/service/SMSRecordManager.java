package tw.tw360.service;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.SMSRecordDao;
import tw.tw360.dto.SMSRecord;

/**
 * 簡訊紀錄
 * @version 1.0
 * */
@Service
@Transactional
public class SMSRecordManager {
	@Autowired
    private SMSRecordDao smsRecordDao;
	
	/**
	 * 用簡訊ID找簡訊
	 * */
	public SMSRecord findByMessageId(String messageId){
		return smsRecordDao.findByMessageId(messageId);
	}
	
	/**
	 * 儲存簡訊
	 * */
	public SMSRecord add(SMSRecord smsRecord){
		return smsRecordDao.add(smsRecord);
	}
	
	/**
	 * 儲存簡訊 回報狀態
	 * */
	public int updateReport(String messageId,String reportStatus, String sourceProdId, String sourceMessageId, Timestamp reportTime){
		return smsRecordDao.updateReport(messageId, reportStatus, sourceProdId, sourceMessageId, reportTime);
	}
	
}
