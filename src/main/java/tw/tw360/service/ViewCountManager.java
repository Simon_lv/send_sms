package tw.tw360.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.ViewCountDao;
import tw.tw360.dto.ViewCount;



/**
 * @version 1.0
 * */
@Service
@Transactional
public class ViewCountManager {
	
	@Autowired
    private ViewCountDao ccDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ViewCount findById(int id){
		return ccDao.findById(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public ViewCount findByPlatform(String platform){
		return ccDao.findByPlatform(platform);
	}
	
	public ViewCount add(final ViewCount br) {
		return ccDao.add(br);
	}
	
	public int updateCount(String platform, int count) {
		return ccDao.updateCount(platform, count);
	}
}
