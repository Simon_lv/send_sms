package tw.tw360.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.PrizeSnDao;
import tw.tw360.dto.PrizeSn;



/**
 * @version 1.0
 * */
@Service
@Transactional
public class PrizeSnManager {
	
	@Autowired
    private PrizeSnDao prizeDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public PrizeSn findById(int id){
		return prizeDao.findById(id);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public PrizeSn getAvailableSn(int prizeId) {
		return prizeDao.getAvailableSn(prizeId);
	}
	
	public int updateStatus(long id, int status) {
		return prizeDao.updateStatus(id, status);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Map<String, Object>> countByPrize() {
		return prizeDao.countByPrize();
	}
}
